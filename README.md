# Verifica 5Bi 07 maggio 2021

### Materia: TPSIT
### Argomento: Docker

## Diagramma
![Diagramma soluzione](./Verifica%205Bi%2007%20maggio%202021.png) 

## Istruzioni
1. Creare su gitlab.com un progetto nel quale salvare il lavoro svolto con nome *NomeCognomeDocker20210517*, aggiungere **andtrentini** come **mantainer** del progetto.

2. Clonare il presente progetto sul proprio computer, rimuovere l'origine remota corrente ed aggiungere
il progetto creato precedentemente come origine remota.

3. Eseguire commit e push regolarmente (max ogni 10 minuti).

4. Consegnare il link del progetto sul compito classroom. 

### Struttura delle directory

    - Verifica5Bi20210507  
      |  
      +- registro-angular-app: applicazione angular da utilizzare per lo sviluppo  
      |  
      +- registro-sql-server:  file sql per la creazione del database  
      |  
      +- registro-ws-server:   web service realizzato con Python + Flask  
  
### Punto1: Realizzare i container per i servizi web server, ws server e sql server
  
  
#### ws server
> Immagine da utilizzare: **python**  
> Nome del container: **registro-ws-server**  
> Nome della directory dell'applicazione **/app**  
> La directory contiene il file requirements.txt nel quale sono presenti i moduli necessari per il funzionamento del progetto  
> Dopo aver copiato i fle nella directory dell'applicazione del container inserire in Dockerfile le istruzioni per installare i packages utilizzati dal web service **(pip install -r requirements.txt)**  
> Il container deve essere connesso ad una rete interna con il container sql server ed accessibile all'indirizzo http://localhost:5000  
> E' necessario fornire al web service la password dell'utente **registro** attraverso docker secret  
> Aggiungere un terminale al container
  
#### sql server
> Immagine da utilizzare: **mysql:latest**  
> Nome del container: **registro-sql-server**  
> Creare un database vuoto durante con nome **registro-sql-server**  
> Il container deve essere connesso ad una rete interna con il container sql server ed accessibile all'indirizzo http://localhost:3308  
> Impostare la password dell'utente *registro* (uguale a **registro**) attraverso docker secret
>
>> Utilizzare le variabili d'ambiente per impostare
>> - **registro** password file  
>> - database name: **registro**  
>> - user: **registro**
  
### angular app
> Immagine da utilizzare: **alpine**  
> Nome del container: **registro-angular-app**  
> Installare nel container **git, node e npm**  
> Installare in global mode **angular**  
> Creare un'applicazione angular nella directory **/app**  
> Aggiungere un terminale al container  
> Utilizzare Visual Studio Code per avviare l'applicazione
